{
    "id": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60379974-c7b2-42b2-be91-449958f7f2fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "cba4fbe5-211c-4d63-be75-853e68d53bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60379974-c7b2-42b2-be91-449958f7f2fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce5b2654-c778-4d6c-acff-caa468de2281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60379974-c7b2-42b2-be91-449958f7f2fa",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "17079582-410b-461f-a8e6-d790c47cded8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "0a86e8be-7e14-4fdc-ba55-96895126529c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17079582-410b-461f-a8e6-d790c47cded8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7aa95eb-d093-4ada-af9a-410daa51a01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17079582-410b-461f-a8e6-d790c47cded8",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "5a6af3d8-96a7-4fc9-8c65-02a5c5b9e581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "216d9a63-bd0f-4c0a-8b9e-d2e3dc776d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6af3d8-96a7-4fc9-8c65-02a5c5b9e581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec9e142-b2f7-48f0-8a7d-2afb3ed37a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6af3d8-96a7-4fc9-8c65-02a5c5b9e581",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "55145b68-513b-40a1-802e-3c1f4a08b3eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "358a20cb-0e57-4a98-afb7-d7f3a0eee118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55145b68-513b-40a1-802e-3c1f4a08b3eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b623348f-3c06-477b-9d2b-be4a065a878e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55145b68-513b-40a1-802e-3c1f4a08b3eb",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "e159ad87-03ca-4d86-a5dd-7412ba299131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "6fff7a79-272f-42c8-ad8c-a97adebabf4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e159ad87-03ca-4d86-a5dd-7412ba299131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6928644-1d8b-4197-a924-e2889162d96e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e159ad87-03ca-4d86-a5dd-7412ba299131",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "ff82bd24-6186-43fe-9cb4-df990e5a61c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "0365bdc0-1316-4cc8-ada6-30198cca5fc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff82bd24-6186-43fe-9cb4-df990e5a61c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b6c9f2-1331-44d0-8ba3-fbff3d7b5ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff82bd24-6186-43fe-9cb4-df990e5a61c1",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "e5a8088c-8b3a-44eb-9e83-c46627cb2a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "980479c8-4492-4311-81d5-75047ebcd4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a8088c-8b3a-44eb-9e83-c46627cb2a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f586a75-acdb-4bf1-9c79-df3e0790c8ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a8088c-8b3a-44eb-9e83-c46627cb2a11",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        },
        {
            "id": "2b4436fc-ea93-47f9-b05b-37b4cc1b0385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "compositeImage": {
                "id": "933a65ab-bd1f-486e-981a-a5419ca8c3e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4436fc-ea93-47f9-b05b-37b4cc1b0385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c9b4ed-99e6-43aa-92cb-ed0a79c1e889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4436fc-ea93-47f9-b05b-37b4cc1b0385",
                    "LayerId": "65123b77-fcf5-49cd-9b1b-f2159c5315ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "65123b77-fcf5-49cd-9b1b-f2159c5315ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bf17770-1c49-4f61-8cb5-866c7dab0243",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -5,
    "yorig": 19
}